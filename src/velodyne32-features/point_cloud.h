/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the Regents of The University of Michigan.
*/

#ifndef POINT_CLOUD_H
#define POINT_CLOUD_H

#include <vx/vx.h>

#include <common/matd.h>
#include <common/zarray.h>

#include "integral_image.h"
#include "interval_image.h"

struct rich_point {
  matd_t *point;
  matd_t *normal;
  double curvature;
};
typedef struct rich_point rich_point_t;

rich_point_t* rich_point_create_zeros();

void rich_point_destroy(rich_point_t *to_destroy);

void rich_point_print(rich_point_t *to_print);

void rich_point_set_zeros(rich_point_t *rp);

void rich_point_set_point(rich_point_t *rp, double* point);

void rich_point_set_normal(rich_point_t *rp, double* normal);

struct point_cloud {
  zarray_t *measurements;
};
typedef struct point_cloud point_cloud_t;

point_cloud_t* point_cloud_create(size_t element_size);

void point_cloud_destroy(point_cloud_t *to_destroy);

void point_cloud_pre_allocate_structures(point_cloud_t *point_cloud, int width, int height);

void point_cloud_add(point_cloud_t *point_cloud, const void *p);

void point_cloud_compute_normals(point_cloud_t *point_cloud, 
				 integral_image_t *integral_image, mati_t *indices,
				 int row_image_radius, int col_image_radius, int min_points);

void point_cloud_compute_normals_using_intervals(point_cloud_t *point_cloud, 
						 integral_image_t *integral_image, 
						 mati_t *indices, interval_image_t *intervals,
						 int min_points);

void point_cloud_draw(point_cloud_t *point_cloud, vx_world_t *vw,
		      char* point_buffer, float *point_color, 
		      int point_size, double normal_scale, 
		      double curvature_threshold);

vx_object_t *vxo_points_with_intensities(vx_resource_t *verts, vx_resource_t *intensities,
					 vx_resource_t *intensity_scale, 
					 char* frag_color_instruction);

#endif
