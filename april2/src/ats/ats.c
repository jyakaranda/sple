/* Copyright (C) 2013-2016, The Regents of The University of Michigan.
All rights reserved.

This software was developed in the APRIL Robotics Lab under the
direction of Edwin Olson, ebolson@umich.edu. This software may be
available under alternative licensing terms; contact the address above.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, see <http://www.gnu.org/licenses/>.
*/

#include "ats.h"
#include "common/zhash.h"
#include "lcmtypes/lcmint64_t.h"

struct ats_tracker{
    zhash_t * contents;
    double smoothing;
};

void on_ats(const lcm_recv_buf_t *rbuf,
             const char *channel, const lcmint64_t * msg, void *userdata) {

    ats_tracker_t * ats = userdata;
    char *s = &strrchr(channel, '_')[1];
    int64_t id = atoll(s);
    int64_t data = msg->data;

    int64_t data_old = 0;
    zhash_get(ats->contents, &id, &data);

    if(data_old)
        data = ats->smoothing*data_old + (1-ats->smoothing)*data;

    zhash_put(ats->contents, &id, &data, NULL, NULL);
}

ats_tracker_t * ats_tracker_create(lcm_t * lcm, const char * regex, double smoothing)
{
    ats_tracker_t * ats = calloc(1,sizeof(ats_tracker_t));
    lcmint64_t_subscribe(lcm, regex, on_ats, ats);

    ats->contents = zhash_create(sizeof(int64_t), sizeof(int64_t),
                                 zhash_uint64_hash, zhash_uint64_equals);

    ats->smoothing = smoothing;

    return ats;
}

int64_t ats_tracker_get(ats_tracker_t * ats, int64_t id){
    int64_t data = 0;
    zhash_get(ats->contents, &id, &data);
    return data;
}
