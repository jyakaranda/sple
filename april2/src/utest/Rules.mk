export CFLAGS_UTEST = $(CFLAGS_COMMON)
export LDFLAGS_UTEST = $(LIB_PATH)/libutest.a -lm -lpthread
export DEPS_UTEST = $(LIB_PATH)/libutest.a

.PHONY: utest_clean

utest:
	@echo $@
	@$(MAKE) -C $(APRIL_PATH)/src/utest -f Build.mk

utest_clean:
	@echo $@
	@$(MAKE) -C $(APRIL_PATH)/src/utest -f Build.mk clean

all: utest

clean: utest_clean

include $(APRIL_PATH)/src/utest/test/Rules.mk
